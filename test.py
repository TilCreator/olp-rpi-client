#!/bin/env python3
import math
import socket
from sys import argv
from time import time
import _rpi_ws281x as ws


MAX_LED_BYTES = 50000
TIMEOUT = 1
LED_CHANNEL = 0
LED_FREQ_HZ = 800000
LED_DMA_NUM = 10
LED_GPIO = int(argv[1])
LED_INVERT = 0


# Create struc
leds = ws.new_ws2811_t()

# Initialize all channels to off
for channum in range(2):
    channel = ws.ws2811_channel_get(leds, channum)
    ws.ws2811_channel_t_count_set(channel, 0)
    ws.ws2811_channel_t_gpionum_set(channel, 0)
    ws.ws2811_channel_t_invert_set(channel, 0)
    ws.ws2811_channel_t_brightness_set(channel, 0)

channel = ws.ws2811_channel_get(leds, LED_CHANNEL)

ws.ws2811_channel_t_count_set(channel, 1)
ws.ws2811_channel_t_gpionum_set(channel, LED_GPIO)
ws.ws2811_channel_t_invert_set(channel, LED_INVERT)
ws.ws2811_channel_t_brightness_set(channel, 255)
ws.ws2811_channel_t_strip_type_set(channel, ws.SK6812_STRIP_RGBW)

ws.ws2811_t_freq_set(leds, LED_FREQ_HZ)
ws.ws2811_t_dmanum_set(leds, LED_DMA_NUM)

# Initialize library with LED configuration.
resp = ws.ws2811_init(leds)
if resp != ws.WS2811_SUCCESS:
    raise RuntimeError(f'ws2811_init failed with code {resp} ({ws.ws2811_get_return_t_str(resp)})')


try:
    for length in list(range(1, 100, 10)) + list(range(100, 1000, 100)) + list(range(1000, 10000, 1000)) + list(range(10000, 100000, 10000)):
        ws.ws2811_channel_t_count_set(channel, math.ceil(length / 4))
    
        resp = ws.ws2811_init(leds)
        if resp != ws.WS2811_SUCCESS:
            raise RuntimeError(f'ws2811_init failed with code {resp} ({ws.ws2811_get_return_t_str(resp)})')

        start_time = time()
    
        for i, data_part in enumerate([(b'\xff' * 4) for i in range(0, math.ceil(length / 4))]):
            ws.ws2811_led_set(channel, i, int(data_part.hex(), 16))

        resp = ws.ws2811_render(leds)
        if resp != ws.WS2811_SUCCESS:
            raise RuntimeError('ws2811_render failed with code {resp} ({ws.ws2811_get_return_t_str(resp)})')

        print(f'{length}bytes, {math.ceil(length / 4)}rgbw-leds, {math.ceil(length / 3)}rgb-leds -> {time() - start_time}sec/update, {1 / (time() - start_time)}fps')
finally:
    ws.ws2811_fini(leds)
    ws.delete_ws2811_t(leds)
