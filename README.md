# OLP Raspberry Pi Client
Install it, connect a LED-Strip to GPIO 18 and remember the hostname or ip, everything else is handled by the Server.

## Install
Install alarm: [rpi1](https://archlinuxarm.org/platforms/armv6/raspberry-pi) [rpi2](https://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2) [rpi3](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3)
```sh
# sh -c "$(curl -fsSL https://gitlab.com/TilCreator/olp-rpi-client/raw/master/install.sh)"
```

### Install yay (optional)
```sh
# pacman -S git sudo go base-devel
# useradd yay -m -G wheel -s /sbin/nologin
# echo "%wheel ALL=(ALL) ALL\n%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
# cd /tmp
# git clone https://aur.archlinux.org/yay.git
# chown -R yay yay
# cd yay
# sudo -u yay makepkg
# pacman -S yay*.pkg*
# echo 'alias yay="sudo -u yay yay"' >> ~/.bashrc
# chmod +x ~/.bashrc
```
