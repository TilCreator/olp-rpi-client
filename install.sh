#!/bin/sh
read -e -p "hostname: " -i "olp-rpi-client-" name
echo $name > /etc/hostname
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
userdel alarm
while true; do
    passwd && break
done
pacman-key --init
pacman-key --populate archlinuxarm
pacman -Syu python git make gcc
cd /srv
git clone https://gitlab.com/TilCreator/olp-rpi-client
cd olp-rpi-client
python -m venv venv
venv/bin/pip install -r requirements.txt
cp olp-client@.service /etc/systemd/system/
cp ping_server@.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable olp-client@18.service
systemctl start olp-client@18.service
read -p "Enable periodic server pinging (y/N)?" CONT
if [ "$CONT" = "y" ]; then
    read -p "Server to ping (bsp: example.com): " address
    addressfile=/srv/olp-rpi-client/address
    echo $address > $addressfile
    systemctl enable ping_server@$(systemd-escape ${addressfile}).service
    systemctl start ping_server@$(systemd-escape ${addressfile}).service
fi
