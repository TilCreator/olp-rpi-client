#!/bin/env python3
import os
import math
import socket
import atexit
import signal
from sys import argv
from time import time
from lockfile import LockFile
import _rpi_ws281x as ws


@atexit.register
def at_exit(_0=None, _1=None):
    ws.ws2811_fini(leds)
    ws.delete_ws2811_t(leds)

    os.remove(f'/tmp/olp-DMA_NUM:{LED_DMA_NUM}')
    os.remove(f'/tmp/olp-CHANNEL:{LED_CHANNEL}')


signal.signal(signal.SIGTERM, at_exit)


try:
    MAX_LED_BYTES = 50000
    HOST = '0.0.0.0'
    PORT = int('23' + argv[1].zfill(2))
    TIMEOUT = 1
    LED_FREQ_HZ = 800000
    LED_GPIO = int(argv[1])
    LED_INVERT = 0
    
    with LockFile('/tmp/olp-lock'):
        for i in range(10, 17, 3):
            if not os.path.exists(f'/tmp/olp-DMA_NUM:{i}'):
                LED_DMA_NUM = i
                break
        else:
            raise RuntimeError('No free DMA channels')
    
        with open(f'/tmp/olp-DMA_NUM:{LED_DMA_NUM}', 'w') as f:
            f.write('')
    
        for i in range(0, 10):
            if not os.path.exists(f'/tmp/olp-CHANNEL:{i}'):
                LED_CHANNEL = i
                break
        else:
            raise RuntimeError('No free CHANNELS')
    
        with open(f'/tmp/olp-CHANNEL:{LED_CHANNEL}', 'w') as f:
            f.write('')


    # Create struc
    leds = ws.new_ws2811_t()
    
    # Initialize all channels to off
    for channum in range(2):
        channel = ws.ws2811_channel_get(leds, channum)
        ws.ws2811_channel_t_count_set(channel, 0)
        ws.ws2811_channel_t_gpionum_set(channel, 0)
        ws.ws2811_channel_t_invert_set(channel, 0)
        ws.ws2811_channel_t_brightness_set(channel, 0)
    
    channel = ws.ws2811_channel_get(leds, LED_CHANNEL)
    
    ws.ws2811_channel_t_count_set(channel, 1)
    ws.ws2811_channel_t_gpionum_set(channel, LED_GPIO)
    ws.ws2811_channel_t_invert_set(channel, LED_INVERT)
    ws.ws2811_channel_t_brightness_set(channel, 255)
    ws.ws2811_channel_t_strip_type_set(channel, ws.WS2811_STRIP_RGB)
    
    ws.ws2811_t_freq_set(leds, LED_FREQ_HZ)
    ws.ws2811_t_dmanum_set(leds, LED_DMA_NUM)
    
    # Initialize library with LED configuration.
    resp = ws.ws2811_init(leds)
    if resp != ws.WS2811_SUCCESS:
        raise RuntimeError(f'ws2811_init failed with code {resp} ({ws.ws2811_get_return_t_str(resp)})')
    
    
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((HOST, PORT))
    

    data = b''
    expected_length = 0
    last_pkg = 0
    while True:
        prev_length = len(data)
    
        data += s.recv(MAX_LED_BYTES - len(data))
    
        if prev_length < len(data):
            last_pkg = time()
    
        if len(data) >= 2 and expected_length == 0:
            expected_length = (data[0] << 8) + data[1]

            if expected_length > MAX_LED_BYTES or expected_length <= 0:
                expected_length = 0
                data = b''

        if expected_length != 0 and expected_length != ws.ws2811_channel_t_count_get(channel) * 3:
            ws.ws2811_channel_t_count_set(channel, math.ceil(expected_length / 3))

            resp = ws.ws2811_init(leds)
            if resp != ws.WS2811_SUCCESS:
                raise RuntimeError(f'ws2811_init failed with code {resp} ({ws.ws2811_get_return_t_str(resp)})')

        if last_pkg > time() - TIMEOUT and len(data) <= 0:
            data = b''
            expected_length = 0
    
        if expected_length + 2 <= len(data):
            for i, data_part in enumerate([data[2:][i:i+3] for i in range(0, len(data[2:]), 3)]):
                data_part += (3 - len(data_part)) * b'\x00'
                ws.ws2811_led_set(channel, i, int(data_part.hex(), 16))

            resp = ws.ws2811_render(leds)
            if resp != ws.WS2811_SUCCESS:
                raise RuntimeError('ws2811_render failed with code {resp} ({ws.ws2811_get_return_t_str(resp)})')

            data = data[expected_length + 2:]
            expected_length = 0
finally:
    at_exit()
