#!/bin/sh
curl "https://$(cat /srv/olp-rpi-client/address)/hostname=$(cat /etc/hostname)/ip=$(ip a s dev eth0 | grep 'inet ' | cut -d 't' -f 2 | cut -d 's' -f 1 | cut -d ' ' -f 2 | cut -d '/' -f 1)" > /dev/null
